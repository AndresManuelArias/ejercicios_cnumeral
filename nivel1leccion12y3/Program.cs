﻿using System;

namespace nivel1leccion12y
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] x = new int[5];

            int[] b = new int[]{0,0,0,0,0};
            int[,] c = new int  [3,3];
            // arreglo multidimen
            int [][] jaggetdArray = new int[3][];
            jaggetdArray[0] = new int[5];
            jaggetdArray[0] = new int[4];
            jaggetdArray[0] = new int[2];
            jaggetdArray[0][0] = 66;
            jaggetdArray[1] = new int[]{0,2,4,6};
            jaggetdArray[2] = new int[]{11,22};
            int [][] jaggetdArray2 = new int [3][];
            jaggetdArray2[2] = new int[5];
            Console.WriteLine("Hello World!");
            /// caracteres

            char caracter ;
            char[] chars = new char[3];
            chars[0]= 'd';
            chars[0]= 'b';

            Console.WriteLine("primer {0}, segundo {1}, tercero {2}",chars[0],chars[1],chars[2]);

            string mensajes = "hola mundo";
            string mensajes1 = "pepito";
            string mensajes2 = mensajes +mensajes1;
            string columns = "Column1\tColumn2\tcolumn3";
            Console.WriteLine(columns);
            columns = "Row1\t\nRow2\t\nRow3";
            Console.WriteLine(columns);
            columns = @"Row1\t\nRow2\t\nRow3";
            Console.WriteLine(columns);
             ///struct
            Direccion personal1;
            Direccion personal2;
            personal1.codPostal = 07450;
            personal1.calle = "Av siempre";
            personal1.ciudad = "";
            Direccion[] personas = new Direccion[2];
            personas[0].calle = "carrera"; 
            Console.WriteLine(personas[0].calle);

            //OPERADOR condicional
            // 1!=1?(Math.Sin(1)):1;
            Console.WriteLine(1!=1?(Math.Sin(1)):1);

            Console.WriteLine("Enter a character:");
            char ch = (char)Console.Read();
            if(Char.IsUpper(ch)){
                Console.Write("Es mayuscula");
            } 
            else if(Char.IsLower(ch))
            {
                Console.Write("Es minuscula");               
            }else if(Char.IsDigit(ch)){
                Console.Write("Es numero");               
            }else {
                Console.Write("No es alfanumerica");               

            }

            switch (ch)
            {
                case 'a':
                    Console.Write("Es a");      
                break;         
                default:
                    Console.Write("no lo se");               
                break;
            }
            // bucles
            int n= 0;
            do {
                Console.Write("se ejecuta una vez aunque sea falso\n");
                n++;
            }while (n < 5);
            n= 0;
            while (n<5)
            {
                Console.Write("no se ejecuta si es falso\n");
                n++;
            }
            for(int i=0;i < 5;i++){
                Console.Write("iterando\n");
            }
        // sentencias de salto
        
            for(int i = 1;i <= 10;i++){
                if(i == 5){
                    continue;
                }
                Console.WriteLine(i);
            }
            for(int aj= 0; aj<10; aj++){
                for(int t = 0 ;t <=20;t++){
                    if(aj ==7){
                        break;// rompe el todo hasta el ciclo anidado donde se encuentre
                    }
                    Console.Write("despues break aj {0} y t {1}\n",aj,t);
                }
            }
            for(int i =0;i <= 5;i++){
                for(int j= 0; i <=20;j++){
                    if(j==7){
                        goto salida;// se define el punto donde se va a romper el ciclo
                    }
                     Console.Write("i {0} y j {1} \n",i,j);
                }
            }
            Console.WriteLine("Se salta esta ñ parte \n");
 
            salida:// se define donde quiere que vuelva nuevamente el ciclo
            Console.WriteLine("Fin del programa ñ\n");
            /// objetos
            Console.WriteLine("Objetos \n");
            Motocicleta moto1 = new Motocicleta();
            int millas = 88;
            int velocidad = 50 ;
            moto1.Manejar(millas, velocidad);
            Console.WriteLine(moto1.Manejar(millas,velocidad));
            
            // metodos
            int numA = 4;
            int resultado1 = Cuadrado(numA);
            int resultado2 = Cuadrado(12);
            int  resultado3 = Cuadrado(resultado1 *3);
            Console.WriteLine(resultado3);
            // uso de objetos
            Persona persona1 = new Persona();
            persona1.Age = 12;
            persona1.Name = "Pepinillo";
            Console.WriteLine("persona1  Age= {0} y  Name {1}",persona1.Age, persona1.Name);

            Persona persona2 = new Persona();
            persona2.Age = 15;
            persona2.Name = "manuel";
            Console.WriteLine("persona2  Age= {0} y  Name {1}",persona2.Age, persona2.Name);
            Console.WriteLine("persona1  Age= {0} y  Name {1}",persona1.Age, persona1.Name);
            // campos, propiedades y constructores
            Color color1 = new Color();
            color1.R = 344;
            color1.G = 344;
            color1.B = 344;
            color1.A = 1000;
            Console.WriteLine("({0},{1},{2},{3})",color1.R,color1.G,color1.B,color1.A );
            Color color2 = new Color(2,3,4);
            Console.WriteLine("({0},{1},{2},{3})",color2.R,color2.G,color2.B,color2.A );
            // static
            Empleado Empleado1 = new Empleado("carma","1234");
            Empleado Empleado2 = new Empleado("carma","1234");
            Empleado Empleado3 = new Empleado("carma","1234");

            Console.WriteLine("Numero de empleados {0}",Empleado.contadorEmpleado );
            // herencia
            Chihuaha perro1 = new Chihuaha();
            Console.WriteLine( perro1.perroLadrar());
            // poliformismo
            Forma forma1 = new Forma();
            Circulo forma2 = new Circulo();
            Rectangulo forma3 = new Rectangulo();
            forma1.Dibujo();
            forma2.Dibujo();
            forma3.Dibujo();
            // evitar el polimorfismo
            C saludo = new C();
            saludo.hola();
            // polimorfismo con clases abstract
            EgiptoGato egiptoGato  = new EgiptoGato();
            egiptoGato.dormir();
            egiptoGato.correr();

        }
        public static int Cuadrado( int i){
            return i*i;
        }
        ///struct
        struct Direccion {
            public int codPostal;
            public string calle;
            public string ciudad;
            public string getDireccion(){
                return calle + "\r\n"+ codPostal + ciudad.ToUpper();
            }
        }

        // objetos
        public class Motocicleta
        {
            // metodos
            public bool Encender(){
                return true;
            }
            public int Manejar(int miles, int speed){
                return miles * speed;
            }
        }
        // uso de objetos
        public struct Persona{
            public string Name;
            public int Age;
        }
        // campos, propiedades y constructores
        public class Color
        
        {
            public Color(){

            }
            public Color(int r,int g,int b){
                R=r;
                G=g;
                B=b;
            }
            private int r;
            private int g;
            private int b;
 

            public int A{ set;get;}
            public int R 
            {
                get{return r;}
                set{r= value; }
            }
            public int G 
            {
                get{ return g;}
                set {
                    if(value <= 255){g = value;}
                    else {g = 255;}
                }
            }
            public int B {
                get { return b;}
                set {
                    if(value <= 255){ b = value;}
                    else { b = 255;}
                }
            }
        }
        // static
        public static class CompaniaEmpleado
        {
            public static void algo(){

            }
            public static void algomas(){

            }
            
        }
        public class Empleado
        {
            public string id;
            public string nombre;
            public static int contadorEmpleado;
            public Empleado(string id,string nombre){
                this.id= id;
                this.nombre= nombre;
                contadorEmpleado++;
            }
        }
        // herencia
        public  class Perro {
            public string perroLadrar(){ return "Perro ladrando.";}
            public  void dormir(){}
            
        }
        public  class Chihuaha:Perro
        {
            public void ChihuahaLadrar(){
                base.perroLadrar();
            }
            // public Chihuaha(string y){
            //     Console.WriteLine(y);
            // }

        }
        // poliformismo
        public class Forma {
            public virtual void Dibujo(){
                Console.WriteLine("Alguna forma");
            }
        }
        public class Circulo :Forma {
            public override void Dibujo(){
                Console.WriteLine("Forma circular");
                base.Dibujo();
            }
        }
        public class Rectangulo :Forma {
            public override void Dibujo(){
                Console.WriteLine("Forma rectangular");
                base.Dibujo();
            }
        }
        // evitar el polimorfismo
        public class A {
            public virtual void hola(){
                Console.WriteLine("Hola soy A");
            }

        }
        public class B:A{
            public sealed override void hola(){// invalida el metodo y lo reimplemanta y lo sella
                Console.WriteLine("Hola soy B");
                base.hola();
            }
        }
        public class C:B {
            public new void hola(){// hereda el metodo sellado y no se purede reimplementar
                Console.WriteLine("hola soy C");
                base.hola();
            }
        }
        public abstract class Gato {
            public   void correr(){
                Console.WriteLine("estoy corriendo");
            }

            public abstract void dormir();
            
        }
        public  class EgiptoGato:Gato
        {
            public   void correr(){
                Console.WriteLine("estoy corriendo egipto");
            }
           public override void dormir(){
                Console.WriteLine("estoy durmiendo");
           }
        
        }
    }
}
