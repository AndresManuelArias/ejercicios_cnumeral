﻿using System;
using System.Collections.Generic;
using System.Collections;
namespace nivel1leccion4
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Hello World!");
            // interfaces video 1
            Tanque tanque1 = new Tanque();
            // IVeiculo veiculo = new IVeiculo();
            validar1(tanque1);
            validar2(tanque1);
            // validar2(veiculo);
            
            int x = 30;
            tanque1.Acelerar(x);
            tanque1.Disparar();
            tanque1.Frenar();

            //implementacion explicita de interfaces video 2
            Ejemplo obj = new Ejemplo();
            IControl c = obj;
            ISuperficie s = obj;
            c.Pintar();
            s.Pintar();

            Cuadro cuadro1 = new Cuadro(30.0f,20.0f);
            IMedidasInglesas eDimension = cuadro1;
            IMedidasMetricas mDimension =  cuadro1;
            Console.WriteLine("Largo(in) ;{0}",eDimension.Largo());
            Console.WriteLine("Largo(in) ;{0}",eDimension.Largo());
            Console.WriteLine("Largo(cm) ;{0}",mDimension.Largo());
            Console.WriteLine("Largo(cm) ;{0}",mDimension.Largo());
            // colecciones video 3
            Console.WriteLine("List");
            var orden = new List<string>{"primero","segundo","tercero","cuarto"};
            Console.WriteLine(orden[0]);
            orden.Add("quinto");
            orden.Add("sexto");
            orden.Remove("segundo");
            foreach (var estado in orden)
            {
                Console.WriteLine(estado+ " \n");
            }
            Console.WriteLine("ArrayList");
            ArrayList ordenArrayList = new ArrayList();
            // diccionarios
            Console.WriteLine("diccionarios");

            Dictionary<string, Elemento> elementos = new Dictionary<string,Elemento>{
                {"k", new Elemento("K","Potasio",19)},
                {"Ca", new Elemento("Ca","Calsio",20)}
            };
            foreach (KeyValuePair<string,Elemento> kvp in elementos)
            {
                Elemento ElElmento = kvp.Value;
                Console.WriteLine("clave:"+kvp.Key);
                Console.WriteLine("valores:"+ElElmento.Simbolo+" "+ElElmento.Nombre);
            }

            //encapsulamiento de metodos delegar video 4
              Console.WriteLine("delegado");
            Del Manejador = delegadoEjemplo;
            Manejador("hola delegado");
            delegadoEjemplo2(1,2,Manejador);
            Del hiDel, byeDel, multiDel, multiMinusHidel;
            hiDel = Hola;
            byeDel = Adios;
            multiDel = hiDel + byeDel;//al sumarse esta guardando varias funciones que 
            multiMinusHidel = multiDel -hiDel;//le quita a multidel la funcion hiDel

            Console.WriteLine("Invoking delegate hiDel:");
            hiDel("A");
            Console.WriteLine("Invoking delegate byeDel:");
            byeDel("B");
            Console.WriteLine("Invoking delegate multiDel:");
            multiDel("C");
            Console.WriteLine("Invoking delegate multiMinusHidel:");
            multiMinusHidel("D");
            LibroDB libroLista = new LibroDB();
            AddLibros(libroLista);
            Console.WriteLine("Titulos de los libros ocn portada de papel");
            libroLista.ProcesarPortada(ImprimirTitulo);

            Total totalLibros = new Total();
            libroLista.ProcesarPortada(totalLibros.PrecioTotal);
            Console.WriteLine("El precio de los libros con portada es {0}",totalLibros.total);
            // Detección de eventos video 5
            Console.WriteLine("Detección de eventos video 5");
            Circulo c1 = new Circulo(54);
            Circulo c2 = new Circulo(88);
            c1.CirculoCambio += ManejadorCirculoCambio;
            c2.CirculoCambio += ManejadorCirculoCambio;

             c1.Dibujar();
             c2.Dibujar(); 
             c1.Actualizar(89);
             c2.Actualizar(87); 
             //     valor de referencia video 6  
            Console.WriteLine("Valor de referencia video 6");

             int x2 = 1;
              Metodo( ref x2);
             Console.WriteLine(x2);
             ArregloNumeros arreglo= new ArregloNumeros();
            Console.WriteLine("Secuencia original "+arreglo.Cadena());
             int numero  = 16;
             ref int x3 = ref arreglo.BuscaNumero(numero);
             x3 *= 2;
             Console.WriteLine("Nueva secuencia "+arreglo.Cadena());
        }
        // valor por referencia video 6
        static int Metodo(ref int numero){
           return numero += 44;
        }
        class ArregloNumeros{
            int[] numeros = {1,2,3,7,15,31,63,127,255,511,1023};
            public ref int BuscaNumero(int objetivo){
                for(int i =0;i < numeros.Length; i++){
                    if(numeros[i] >= objetivo){
                        return ref numeros[i];
                    }
                }
                return ref numeros[0];
            }
            public string Cadena(){
                return string.Join(" ",numeros);
            }
        }
        // ejemplo evento
        private static void ManejadorCirculoCambio(object sender,CirculoEventArgs AreaCambio){
            Circulo s = (Circulo)sender;
            Console.WriteLine("Nuevo evento, El area del circulo ahora es {0}",AreaCambio.NuevaArea);
            s.Dibujar();
        }
        public class Circulo
        {
            private  double radio;
            public double Area { get;set;}

            public Circulo (double rad){
                radio= rad;
                Area = 3.14*radio*radio;
            }
            public void Actualizar( double rad){
                radio= rad;
                Area = 3.14*radio*radio;
                OnCirculoCambio( new CirculoEventArgs(Area));
            }
            public void Dibujar(){
                Console.WriteLine("dibujar un circulo");
            }
            public event EventHandler<CirculoEventArgs>CirculoCambio;
            protected void OnCirculoCambio(CirculoEventArgs AreaCambio)
            {
                EventHandler<CirculoEventArgs> Manejador = CirculoCambio;
                if(Manejador != null){
                    Manejador(this,AreaCambio);
                }
            }
            
        }
        public class CirculoEventArgs:EventArgs{
            public double NuevaArea{get;set;}
            public CirculoEventArgs(double rad){
                NuevaArea = rad;
            }
        }
        //encapsulamiento de metodos delegar video 4
        public delegate void Del(string mensaje);
        public static void delegadoEjemplo(string mensaje){
            Console.WriteLine(mensaje);
        }
        static void delegadoEjemplo2(int param1, int param2, Del llamada){
            llamada("El numero es:"+(param1+param2).ToString());
        }
        static void Hola(string s){
            Console.WriteLine("hello, {0}!",s);
        }
        static void Adios(string s){
            Console.WriteLine("Goodbye, {0}!",s);
        }
        public class Libro{
            public string Titulo;
            public string Autor;
            public decimal precio;
            public bool PortadaPapel;
            public Libro(string Titulo,string Autor,decimal precio,bool PortadaPapel){
                Titulo = Titulo;
                Autor = Autor;                
                precio = precio;
                PortadaPapel = PortadaPapel;

            }
        }
        static void ImprimirTitulo(Libro b){
            Console.WriteLine("{0}", b.Titulo);
        }
        static void AddLibros(LibroDB libroDB){
            libroDB.AddLibro("titulo1","autor1",1234,true);
            libroDB.AddLibro("titulo2","autor2",1234,false);
            libroDB.AddLibro("titulo3","autor3",1234,true);
        }
        public delegate void ProcesarLIbro (Libro libro);
        public class LibroDB{
         
            ArrayList lista = new ArrayList();
            public void AddLibro(string Titulo,string Autor,decimal precio,bool PortadaPapel ){
                lista.Add(new Libro(Titulo,Autor,precio,PortadaPapel));
            }
            public void ProcesarPortada(ProcesarLIbro procesarLIbro){
                foreach (Libro b in lista)
                {
                    if(b.PortadaPapel){
                        procesarLIbro(b);
                    }
                }
            }
        }
        public class Total {
            public decimal total;
            public void PrecioTotal(Libro libro){
                total += libro.precio;
            }
        }
//--- termina delegar
        static void validar1(IVeiculo a){
            Console.WriteLine("Esto es un vehiculo");
        }
        static void validar2(IArmaDeGuerra a){
            Console.WriteLine("Esto es un arma de guerra");
        }
        // interfaces

        interface IVeiculo{
            void Acelerar(int kmh);
            void Frenar();
            void Girar(int angulos);
        }
        interface IArmaDeGuerra{
            void Apuntar();
            void Disparar();
        }
        class Tanque:IVeiculo,IArmaDeGuerra{
                
            public void  Acelerar(int kmh){
                Console.WriteLine("Tanque acelerando a {0}",kmh);
            }   
            public void  Frenar(){
                Console.WriteLine("Tanque frenado");
            }
            public void   Girar(int angulos){
                Console.WriteLine("Tanque girando a {0}", angulos);
            }
            public void  Apuntar(){
                Console.WriteLine("Tanque apuntando");
            }
            public void  Disparar(){
                Console.WriteLine("Tanque disparando");
            }

        }
        //implementacion explicita de interfaces video 2
        interface IControl {
            void Pintar();
        }
        interface ISuperficie{
            void Pintar();
        }
        public class Ejemplo:IControl, ISuperficie{
            void IControl.Pintar(){
                Console.WriteLine("IControl.Pintar");
            } 
            void ISuperficie.Pintar(){
                Console.WriteLine("ISuperficie.Pintar");
            }        
        }
        interface IMedidasInglesas {
            float Largo();
            float Ancho();
        }
        interface IMedidasMetricas {
            float Largo();
            float Ancho();
        }
        public class Cuadro:IMedidasInglesas,IMedidasMetricas{
            public float LargoPulgadas;
            public float AnchoPulgadas;
            public Cuadro(float LargoPulgadas, float AnchoPulgadas){
                this.LargoPulgadas =LargoPulgadas;
                this.AnchoPulgadas =AnchoPulgadas;
            }
            float IMedidasInglesas.Largo(){return LargoPulgadas;}
            float IMedidasInglesas.Ancho(){return AnchoPulgadas;}
            float IMedidasMetricas.Largo(){return LargoPulgadas * 2.54f;
            }
            float IMedidasMetricas.Ancho(){return AnchoPulgadas * 2.54f;
            }
        }

        public class Elemento {
            public string Simbolo {get;set;}
            public string Nombre {get;set;}
            public int NumeroAtomico{get;set;}
            public Elemento(string simbolo, string nombre, int numeroatomico){
                Simbolo = simbolo;
                Nombre = nombre;
                NumeroAtomico = numeroatomico;
            }

        }
    }

 
}
