﻿using System.Windows;
namespace leccion1
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            // video 3 use de from

            this.button1 = new System.Windows.Forms.Button();
            // this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(182, 208);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Ejecutar";
            this.button1.UseVisualStyleBackColor = true;
            //video 4 calculadora
            this.button1.Click += Calcular_Click;// este es un delegado que recibe el evento clic
            this.label1 = new System.Windows.Forms.Label();
            // 
            // button2
            // 
            this.button2 = new System.Windows.Forms.Button();

            this.button2.Location = new System.Drawing.Point(0, 208);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 0;
            this.button2.Text = "formulario 2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += irAlOtroFormulario;// este es un delegado que recibe el evento clic

            //textBox2
            //
            this.textBox2 =  new System.Windows.Forms.TextBox();
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(75, 50);
            this.textBox2.TabIndex = 3;
            this.textBox2.Text = "";
            this.textBox2.Location = new System.Drawing.Point(0, 0);
            //label1
            //
            
            this.label1.Location = new System.Drawing.Point(0, 170);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 23);
            this.label1.TabIndex = 1;
            // 
            //label2
            //
            this.label2 = new System.Windows.Forms.Label();

            this.label2.Location = new System.Drawing.Point(0, 150);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 23);
            this.label2.TabIndex = 4;  
            this.label2.Text = "Resulatdo";
            //textBox1
            //
            this.textBox1 =  new System.Windows.Forms.TextBox();
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(75, 10);
            this.textBox1.TabIndex = 2;
            this.textBox1.Text = "";
            this.textBox1.Location = new System.Drawing.Point(0, 20);
            // video 4 
            box = new System.Windows.Forms.GroupBox();
            // Setting the location of the GroupBox
            box.Location = new System.Drawing.Point(75, 20);

            // Setting the size of the GroupBox
            box.Size = new  System.Drawing.Size(75, 150);

            // Setting text the GroupBox
            box.Text = "Select Gender";

            // Setting the name of the GroupBox
            box.Name = "MyGroupbox";
            rDivicion = new System.Windows.Forms.RadioButton();
            rDivicion.AutoSize = true; 
            rDivicion.Text = "/"; 
            rDivicion.Location = new System.Drawing.Point(10, 40); 
            rDivicion.Visible = true; 
            // Setting the name of the GroupBox
            rMultiplicacion = new System.Windows.Forms.RadioButton();
            rMultiplicacion.AutoSize = true; 
            rMultiplicacion.Text = "*"; 
            rMultiplicacion.Location = new System.Drawing.Point(10, 60); 
            rMultiplicacion.Visible = true;     
            // Adding this label to the form 
            // this.Controls.Add(r1);
            rSuma = new System.Windows.Forms.RadioButton();
            rSuma.AutoSize = true; 
            rSuma.Text = "+"; 
            rSuma.Location = new System.Drawing.Point(10, 80); 
            rSuma.Visible = true;   

            rResta = new System.Windows.Forms.RadioButton();
            rResta.AutoSize = true; 
            rResta.Text = "-"; 
            rResta.Location = new System.Drawing.Point(10, 100); 
            rResta.Visible = true;  
            
            rPorcentage = new System.Windows.Forms.RadioButton();
            rPorcentage.AutoSize = true; 
            rPorcentage.Text = "%"; 
            rPorcentage.Location = new System.Drawing.Point(10, 115); 
            rPorcentage.Visible = true;     
     
            ///
            this.SuspendLayout();
            
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.textBox2);
            this.Name = "Form1";
            this.Text = "Form1";
            // Adding groupbox in the form video 4
            this.Controls.Add(box);
            // Adding this control to the GroupBox
            box.Controls.Add(rDivicion);
            box.Controls.Add(rMultiplicacion);
            box.Controls.Add(rSuma);
            box.Controls.Add(rPorcentage);
            this.ResumeLayout(false);
        }

        #endregion
         private System.Windows.Forms.Button button1;
         private System.Windows.Forms.Button button2;
         private System.Windows.Forms.Label label1;
         private System.Windows.Forms.Label label2;
         private System.Windows.Forms.TextBox textBox1;
         private System.Windows.Forms.TextBox textBox2;
         // Creating a GroupBox
        private System.Windows.Forms.GroupBox box ; 
        // Creating radio button
        System.Windows.Forms.RadioButton rDivicion;
        System.Windows.Forms.RadioButton rSuma;
        System.Windows.Forms.RadioButton rResta;
        System.Windows.Forms.RadioButton rMultiplicacion;
        System.Windows.Forms.RadioButton rPorcentage;
    }
}

