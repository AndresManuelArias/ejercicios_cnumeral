﻿namespace leccion1
{
    partial class Form2
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            
              this.button1 = new System.Windows.Forms.Button();
            // this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(182, 208);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "cerrar ventana";
            this.button1.UseVisualStyleBackColor = true;
            //video 4 calculadora
            this.button1.Click += btnCerrar_Click;// este es un delegado que recibe el evento clic
    
            // video 6 leccion1 nivel2 
             //label1
            //
            this.label1 = new System.Windows.Forms.Label();          
            this.label1.Location = new System.Drawing.Point(75, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 23);
            this.label1.TabIndex = 1;
            this.label1.Text = "InitializeComponent";
            this.Controls.Add(this.label1);
            //label2
            //
            this.label2 = new System.Windows.Forms.Label();          
            this.label2.Location = new System.Drawing.Point(75, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 23);
            this.label2.TabIndex = 2;
            this.label2.Text = "prueba";
            this.Controls.Add(this.label2);

            // button2
            // 
            this.button2 = new System.Windows.Forms.Button();

            this.button2.Location = new System.Drawing.Point(1, 1);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 0;
            this.button2.Text = "show";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.DialogResult =  System.Windows.Forms.DialogResult.OK;
            this.Controls.Add(this.button2);

            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.button1);
        }
        public void CreateMyPanel()
        {
            System.Windows.Forms.Panel panel1 = new System.Windows.Forms.Panel();
            System.Windows.Forms.Panel panel2 = new System.Windows.Forms.Panel();
            // Initialize the Panel control.
            panel1.Location = new System.Drawing.Point(0,0);
            panel1.Size = new System.Drawing.Size(264, 152);
            // Set the Borderstyle for the Panel to three-dimensional.
            panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;

            panel2.Location = new System.Drawing.Point(56,72);
            panel2.Size = new System.Drawing.Size(264, 152);
            // Set the Borderstyle for the Panel to three-dimensional.
            panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.button1 = new System.Windows.Forms.Button();
            // this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(182, 208);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "cerrar ventana";
            this.button1.UseVisualStyleBackColor = true;
            //video 4 calculadora
            this.button1.Click += btnCerrar_Click;// este es un delegado que recibe el evento clic
    
            // video 6 leccion1 nivel2 
             //label1
            //
            this.label1 = new System.Windows.Forms.Label();          
            this.label1.Location = new System.Drawing.Point(75, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(75, 23);
            this.label1.TabIndex = 1;
            this.label1.Text = "prueba";
            panel1.Controls.Add(this.label1);
            //label2
            //
            this.label2 = new System.Windows.Forms.Label();          
            this.label2.Location = new System.Drawing.Point(75, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 23);
            this.label2.TabIndex = 2;
            this.label2.Text = "CreateMyPanel";
            panel1.Controls.Add(this.label2);
          //label3
            //
            this.label3 = new System.Windows.Forms.Label();          
            this.label3.Location = new System.Drawing.Point(0, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 23);
            this.label3.TabIndex = 2;
            this.label3.Text = "formulario panel";
            panel1.Controls.Add(this.label3);
            // button2
            // 
            this.button2 = new System.Windows.Forms.Button();

            this.button2.Location = new System.Drawing.Point(1, 1);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 0;
            this.button2.Text = "show";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.DialogResult =  System.Windows.Forms.DialogResult.OK;
            panel1.Controls.Add(this.button2);

            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            panel1.Controls.Add(this.button1);
            panel2.Controls.Add(panel1);
            this.Controls.Add(panel1);
            this.Controls.Add(panel2);
        }
        #endregion
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;



    }
}

