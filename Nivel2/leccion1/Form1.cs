﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace leccion1
{
    public partial class Form1 : Form
    {
        Timer timer = new Timer();
        public Form1()
        {
            InitializeComponent();
            // video 3 use de from

            timer.Tick += new EventHandler(timer1_Tick); // it better tu do this by double Tab Key
            timer.Interval = 1;             
            timer.Enabled = true;
            
        }
        private void Calcular_Click(Object sender,EventArgs e){
            Double a = Convert.ToDouble(textBox2.Text);
            Double b = Convert.ToDouble(textBox1.Text);
            Console.WriteLine("a:"+a+" b:"+b);
            double  resultado = 0.0f;
            if(rDivicion.Checked == true){resultado = a/b;}
             if(rMultiplicacion.Checked == true){resultado = a*b;}
             if(rSuma.Checked == true){resultado = a+b;}
             if(rResta.Checked == true){resultado = a-b;}
             if(rPorcentage.Checked == true){resultado = a%b;}
        

         
            this.label2.Text = resultado.ToString();

        }
        private void timer1_Tick(Object sender,EventArgs e){
            // video 3 use de from
           
            label1.Text =DateTime.Now.ToShortDateString()+"-"+ DateTime.Now.ToShortTimeString();
        }
        private void irAlOtroFormulario(Object sender,EventArgs e){
            Console.WriteLine(textBox2.Text);
            Form2 secondForm = new Form2(textBox2.Text);
            secondForm.Mensaje = textBox1.Text;
            this.Visible = false;
            DialogResult  resultado =  secondForm.ShowDialog ();
            if(resultado == DialogResult.OK){
                this.label2.Text = secondForm.Mensaje;

            }
            secondForm.Dispose();
            this.Visible = true;
        }
    }
}
//https://www.homeandlearn.co.uk/csharp/csharp_s13p1.html